import java.util.Random;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;



class Task1 {

    public static void main(String[] args) {

        for (int i = 0; i < 20; i++) {
            System.out.println(getRandomNumberInRange(0, 20));

        }

    }

    private static int getRandomNumberInRange(int min, int max) {

        if (min >= max) {
            throw new IllegalArgumentException("максимальное знач. должно быть больше минимального");
        }

        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }

}



class WriteToFile {

    private static final String FILENAME = "C:\\Users\\Илья\\IdeaProjects\\TestWork1.txt";

    public static void main(String[] args) {

        try (BufferedWriter bw = new BufferedWriter(new FileWriter(FILENAME))) {

            String content = "Это будет написано в файле\n";

            bw.write(content);


            System.out.println("Файл создан");

        } catch (IOException e) {

            e.printStackTrace();

        }

    }

}


